import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

@IonicPage()

@Component({
	selector: 'page-cart',
	templateUrl: 'cart.html',
})
export class CartPage {
	cartItems: any[] = [];
	total: any;
	showEmptyCartMessage: boolean = false;

	constructor(
		private _navCtrl: NavController,
		private _storage: Storage,
		private _navParams: NavParams,
		private _viewCtrl: ViewController,
		private _toastCtrl: ToastController
	) {
		this.total = 0.0;

		this._storage.ready().then(() => {

			this._storage.get("cart").then((data) => {
				this.cartItems = data;
				console.log(this.cartItems);

				if (this.cartItems.length > 0) {

					this.cartItems.forEach((item, index) => {
						this.total = this.total + (item.product.price * item.qty)
					})

				} else {

					this.showEmptyCartMessage = true;

				}


			})

		})
	}

	removeFromCart(item, i) {

		let price = item.product.price;
		let qty = item.qty;

		this.cartItems.splice(i, 1);

		this._storage.set("cart", this.cartItems).then(() => {

			this.total = this.total - (price * qty);

		});

		if (this.cartItems.length == 0) {
			this.showEmptyCartMessage = true;
		}
	}

	closeModal() {
		this._viewCtrl.dismiss();
	}

	checkout() {
		this._storage.get("userLoginInfo").then((data) => {
			if (data != null) {
				this._navCtrl.push("CheckoutPage");
			} else {
				this._navCtrl.push("LoginPage", { next: "CheckoutPage" })
			}
		})

	}

	changeQty(item, i, change) {

		let price = 0;
		let qty = 0;

		price = parseFloat(item.product.price);
		qty = item.qty;

		if (change < 0 && item.qty == 1) {
			return;
		}

		qty = qty + change;
		item.qty = qty;
		item.amount = qty * price;

		this.cartItems[i] = item;

		this._storage.set("cart", this.cartItems).then(() => {

			this._toastCtrl.create({
				message: "Cart Updated.",
				duration: 2000,
				showCloseButton: true
			}).present();

		});
	}
}
