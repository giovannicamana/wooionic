import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, ModalController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import * as WC from 'woocommerce-api';

@IonicPage()

@Component({
	selector: 'page-product-detail',
	templateUrl: 'product-detail.html',
})
export class ProductDetailPage {
	product: any;
	WooCommerce: any;
	reviews: any[] = [];

	constructor(
		private navCtrl: NavController,
		private navParams: NavParams,
		private storage: Storage,
		private toastCtrl: ToastController,
		private modalCtrl: ModalController
	) {
		this.product = this.navParams.get("product");
		console.log(this.product);

		this.WooCommerce = WC({
			url: "http://woo.modernconceptbuilders.com",
			consumerKey: "ck_2e1a60e4cdc45f0fea84b4a771ea96081745adcc",
			consumerSecret: "cs_2846df38b4e763efeac3700d643fcf78c5762eb6"
		});

		this.WooCommerce.getAsync('products/' + this.product.id + '/reviews').then((data) => {

			this.reviews = JSON.parse(data.body).product_reviews;
			console.log(this.reviews);

		}, (err) => {
			console.log(err);
		});
	}

	addToCart(product) {

		this.storage.get("cart").then((data) => {

			if (data == null || data.length == 0) {
				data = [];

				data.push({
					"product": product,
					"qty": 1,
					"amount": parseFloat(product.price)
				})
			} else {

				let added = 0;

				for (let i = 0; i < data.length; i++) {

					if (product.id == data[i].product.id) {
						let qty = data[i].qty;

						console.log("Product is already in the cart");

						data[i].qty = qty + 1;
						data[i].amount = parseFloat(data[i].amount) + parseFloat(data[i].product.price);
						added = 1;
					}

				}

				if (added == 0) {
					data.push({
						"product": product,
						"qty": 1,
						"amount": parseFloat(product.price)
					})
				}

			}

			this.storage.set("cart", data).then(() => {
				console.log("Cart Updated");
				console.log(data);

				this.toastCtrl.create({
					message: "Cart Updated",
					duration: 3000
				}).present();

			});

		});
	}

	openCart() {
		this.modalCtrl.create("CartPage").present();
	}
}
