import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';

import * as WC from 'woocommerce-api';

@IonicPage()
@Component({
	selector: 'page-search',
	templateUrl: 'search.html',
})
export class SearchPage {
	searchQuery: string = "";
	WooCommerce: any;
	products: any[] = [];
	page: number = 2;

	constructor(
		private _navCtrl: NavController,
		private _navParams: NavParams,
		private _toastCtrl: ToastController
	) {
		this.searchQuery = this._navParams.get("searchQuery");

		this.WooCommerce = WC({
			url: "http://woo.modernconceptbuilders.com",
			consumerKey: "ck_2e1a60e4cdc45f0fea84b4a771ea96081745adcc",
			consumerSecret: "cs_2846df38b4e763efeac3700d643fcf78c5762eb6"
		});

		this.WooCommerce.getAsync("products?filter[q]=" + this.searchQuery).then((searchData) => {
			this.products = JSON.parse(searchData.body).products;
		});
	}

	loadMoreProducts(event) {

		this.WooCommerce.getAsync("products?filter[q]=" + this.searchQuery + "&page=" + this.page).then((searchData) => {
			this.products = this.products.concat(JSON.parse(searchData.body).products);

			if (JSON.parse(searchData.body).products.length < 10) {
				event.enable(false);

				this._toastCtrl.create({
					message: "No more products!",
					duration: 5000
				}).present();

			}

			event.complete();
			this.page++;
		});
	}

}
