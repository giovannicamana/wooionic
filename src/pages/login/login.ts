import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController, Events } from 'ionic-angular';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
	selector: 'page-login',
	templateUrl: 'login.html',
})
export class LoginPage {
	username: string;
	password: string;

	constructor(
		private _navCtrl: NavController,
		private _navParams: NavParams,
		private _http: Http,
		private _toastCtrl: ToastController,
		private _storage: Storage,
		private _alertCtrl: AlertController,
		private _events: Events
	) {
		this.username = "";
		this.password = "";
	}
	signup() {
		this._navCtrl.push('SignupPage');
	}
	login() {
		this._http.get("http://woo.modernconceptbuilders.com/api/auth/generate_auth_cookie/?insecure=cool&username=" + this.username + "&password=" + this.password)
			.subscribe((res) => {
				console.log(res.json());

				let response = res.json();

				if (response.error) {
					this._toastCtrl.create({
						message: response.error,
						duration: 5000
					}).present();
					return;
				}

				this._storage.set("userLoginInfo", response).then((data) => {

					this._alertCtrl.create({
						title: "Login Successful",
						message: "You have been logged in successfully.",
						buttons: [{
							text: "OK",
							handler: () => {

								this._events.publish("updateMenu");

								if (this._navParams.get("next")) {
									this._navCtrl.push(this._navParams.get("next"));
								} else {
									this._navCtrl.pop();
								}
							}
						}]
					}).present();
				});
			});
	}
}
